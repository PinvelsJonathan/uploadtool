var fs=require('fs')
var config=require('./config.js')
var request = require('request'); 
var rp=require('request-promise')
var main_domain=config.APIDomain
var get_location_url="/api/locations"
var get_city_url="/api/cities"
var get_post_url="/api/posts"
var image_url="/api/resources/images"
var create_user_endpoint="/api/cms/users"
var login_url="/api/login"
var update_profile ="/api/users/me"
var video_url="/api/resources/videos"
var create_post_url="/api/posts"
var cms_get_post_url=`/api/cms/posts/`
var cms_list_post_url=`/api/cms/posts`
var url="https://translation.googleapis.com/language/translate/v2"


function list_api(url,cursor,hit=10){
    var option={
        uri:url+"?cursor="+cursor+"&hits="+hit,
        json:true
    }
    return rp(option)
}

exports.get_location_api=function (cursor,hit=10){
    var to= main_domain+get_location_url
     return list_api(to,cursor,hit)
}
exports.get_city_api=function(cursor,hit=10){
    var to= main_domain+get_city_url
    return list_api(to,cursor,hit)
}
exports.get_post_api=function(cursor,hit=10){
    var to=main_domain+get_post_url
    return list_api(to,cursor,hit)
}
/**
 * 
 * @param {string} type 
 * @param {string} path 
 * @param  {(err,response,body)=>{}} callback 
 */
exports.upload_file=function(type,path,callback){
    if(type=="images")
        upload_image(main_domain+image_url,path,callback)
}


exports.upload_video=function(path){
    var option={
        uri:main_domain+video_url,
        json:true,
        method:'POST'
    }
   return rp(option).then(res=>{
       //console.log(res)
        var upload_option={
            uri:res.host,
            method:'POST',
            formData:{
                key:res.path,
                policy:res.policy,
                OSSAccessKeyId:res.access_id,
                success_action_status :'200',
                callback:res.callback,
                signature:res.signature,
                file: fs.createReadStream(path)
              
            },
            json:true
        }

        //console.log(upload_option)
        return rp(upload_option)
    })


    /*
     'key' : responseData.path,
      'policy': responseData.policy,
      'OSSAccessKeyId': responseData.access_id,
      'success_action_status' : '200',
      'callback' : responseData.callback,
      'signature': responseData.signature,
    */

}
/**
 * 
 * @param {to_url} url 
 * @param {file_path} path 
 * @param {(err,httpResonse,body)=>{}} callback 
 */
function upload_image(url,path,callback){
    var formData = {
        file: fs.createReadStream(path)
      };
    request.post({url:url,formData:formData},callback)
}


exports.createUser= function(email,name,pw){
    var option={
        uri:main_domain+create_user_endpoint,
        json:true,
        method:'POST',
        headers:{
            'authorization':config.token,
            'Content-Type':'application/json'
        },
        body:{
            email:email,
            name:name,
            password:pw
        }
    }
    return rp(option)
}

exports.login_user=function(email,pw){
    var option={
        uri:main_domain+login_url,
        json:true,
        method:'POST',
        headers:{
            'Content-Type':'application/json'
        },
        body:{
            email:email,
            password:pw
        }
    }
    return rp(option)
}

exports.update_profile=function(obj,token){
    var option={
        uri:main_domain+update_profile,
        method:'PUT',
        headers:{
            'X-REDSO-SECURITY-ACCESS-TOKEN':token,
            'Content-Type':'application/json'
        },
        body:JSON.stringify(obj)
    }
    if(useIP){
        option.uri='http://'+hostIP+update_profile
        option.headers.Host='server.pinvels.com'
    }
    //console.log(option)
    return rp(option)
}

exports.createPost=function(token,obj){
    var option={
        uri:main_domain+create_post_url,
        body:obj,
        headers:{
            'X-REDSO-SECURITY-ACCESS-TOKEN':token,
            'Content-Type':'application/json'
        },
        json:true,
        method:'POST'
    }
    //console.log(option)
    return rp(option)
}

exports.getCmsPostDetail=function(postid){
    var option={
        uri:main_domain+cms_get_post_url+postid,
        json:true,
        headers:{
            Authorization:config.cmsToken
        }
    }
    return rp(option)
}



exports.translateOneLangFromHant=function(language,q){
    var _option={
        uri:url,
        json:true,
        qs:{
            q:q,
            target:language,
            source:"zh-CN",
            key:config.googleAPITOKEN
        }
    }
    return rp(_option)
}
exports.updateCmsPost=function(postDetailObj){
    var option={
        uri:main_domain+cms_get_post_url+postDetailObj.id,
        method:'PUT',
        headers:{
            Authorization:config.cmsToken
        },
        body:postDetailObj,
        json:true
    }
    return rp(option)
}

exports.getCmsPost=function(cursor,hits,count=false){
    var option={
        uri:main_domain+cms_list_post_url+`?offset=${cursor}&hits=${hits}&count=${count}`,
        method:'Get',
        headers:{
            Authorization:config.cmsToken
        },
        json:true
    }
    return rp(option)



}


