var api=require('./api.js')
var config=require('./config.js')
var weighted = require('weighted')
var postDetail={}
const readline = require('readline');

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});
var resultPostPoint=""
var date =new Date()
date.setHours(getRandomInt(0,23),getRandomInt(0,59),getRandomInt(0,59))
function getRandomInt(min,max){
return Math.floor(Math.random()*(max-min+1))+min
}
var obj={}
api.login_user(config.email,config.pw).then(profile=>{
    console.log('info- login success')
    obj.profile=profile
    return api.upload_video(config.path)
}).then(videoRes=>{
    console.log('info- upload success')
    obj.videoRes=videoRes
    var body_obj={
        content:config.content,
        locale:config.locale,
        location_id:config.locationid,
        title:config.title,
        suggested_location:"",
        video:videoRes
    }
    return api.createPost(obj.profile.access_token,body_obj).then(postData=>{
        console.log('Info--create Post Done')
        console.log('Info--waiting 5s...')

        return new Promise(done=>setTimeout(done,5000)).then(a=> {return api.getCmsPostDetail(postData.id)})
    })


}).then(postd=>{
    postDetail=postd
    return postd
    console.log(postd)
}).then(postd=>{
    return api.translateOneLangFromHant('zh-TW',postd.title.zh_hans).then(transR=>{           
        title_hant=transR.data.translations[0].translatedText
        //console.log(title_hant)
        return api.translateOneLangFromHant('en',postd.title.zh_hans).then(transR=>{
             title_en=transR.data.translations[0].translatedText
             //console.log(title_en)
            return ""
        })
    }).then(_=>{
        return api.translateOneLangFromHant('en',postd.content.zh_hans).then(transR=>{
            content_en=transR.data.translations[0].translatedText
            //console.log(content_en)
            return api.translateOneLangFromHant('zh-TW',postd.content.zh_hans).then(transR=>{
                content_hant=transR.data.translations[0].translatedText
                //console.log(content_hant)
                return""
            })
        })
    })
}).then(_=>{
    resultPostPoint=randomePoint()
    console.log(`------------------------------------------`)
    rl.question( `date: ${date.toLocaleString()}\nrandomPointResult:\n${resultPostPoint}\n----\ntranslated result:\ntitle_en:${title_en}\ntitle_zh_hant:${title_hant}\ncontent_en:${content_en}\ncontent_hant:${content_hant}\nconfirm?(y,n,t(modify title),c(modify content),ct(both)`, (answer) => {
        // TODO: Log the answer in a database
       switch(answer){
           case 'y':
            updatePostObj()
            api.updateCmsPost(postDetail).then(result=>{
                console.log(`updated status:${result.state}`)
            })
            rl.close()
            break
            case 'n':
            console.log('aborted. post has been created but not updated')
            rl.close()
            break
            case 't':
            var t_hant=null
            var t_en=null
            rl.question(`enter title_zh_hant ,empty to skip:`,(a)=>{
                if(a!="")
                    t_hant=a
                rl.question(`enter title_en ,empty to skip:`,(a)=>{
                    if(a!="")
                        t_en=a
                    if(t_hant!=null)
                        title_hant=t_hant
                    if(t_en!=null)
                        title_en=t_en
                    updatePostObj()
                    api.updateCmsPost(postDetail).then(result=>{
                        console.log(`updated status:${result.state}`)
                    })
                    rl.close()
                } )
            })            
            break
            case'c':
            var c_hant=null
            var c_en=null
            rl.question(`enter content_zh_hant, empty to skip:`,(a)=>{
                if(a!="")
                    c_hant=a
                rl.question(`enter content_en ,empty to skip:`,(a)=>{
                    if(a!="")
                        c_en=a
                    if(c_hant!=null)
                        content_hant=c_hant
                    if(c_en!=null)
                        content_en=c_en
                    updatePostObj()
                    api.updateCmsPost(postDetail).then(result=>{
                        console.log(`updated status:${result.state}`)
                    })
                    rl.close()
                } )
            })           
            break
            case 'ct':
            var t_hant=null
            var t_en=null
            var c_hant=null
            var c_en=null
            rl.question(`enter title_zh_hant, empty to skip:`,(a)=>{
                if(a!="")
                    t_hant=a
                rl.question(`enter title_en ,empty to skip:`,(a)=>{
                    if(a!="")
                        t_en=a
                    if(t_hant!=null)
                        title_hant=t_hant
                    if(t_en!=null)
                        title_en=t_en
                        rl.question(`enter content_zh_hant, empty to skip:`,(a)=>{
                            if(a!="")
                                c_hant=a
                            rl.question(`enter content_en, empty to skip:`,(a)=>{
                                if(a!="")
                                    c_en=a
                                if(c_hant!=null)
                                    content_hant=c_hant
                                if(c_en!=null)
                                    content_en=c_en
                                updatePostObj()
                                api.updateCmsPost(postDetail).then(result=>{
                                    console.log(`updated status:${result.state}`)
                                })
                                rl.close()
                            } )
                        })
                } )
            })      

            break

            default:
            console.log('unexpected input.aborted. post has been created but not updated')
            rl.close()
       }
      });
})

function updatePostObj(){
    if(config.published){
        postDetail.state='published'
    }
    postDetail.point=resultPostPoint
    postDetail.title.en=title_en
    postDetail.title.zh_hant=title_hant
    postDetail.title.es=title_en
    postDetail.title.fr=title_en
    postDetail.title.ja=title_en
    postDetail.title.ko=title_en
    postDetail.content.en=content_en
    postDetail.content.zh_hant=content_hant
    postDetail.content.es=content_en
    postDetail.content.fr=content_en
    postDetail.content.ja=content_en
    var string=""
    lang_array.forEach((e)=>{
        string+=postDetail.location.name[e]        
        string+='\n'
        string+=postDetail.location.city.name[e]
        string+='\n'
        string+=postDetail.location.country.name[e]
    })
    postDetail.content.ko=string
    postDetail.mtime=date.getTime()
}


/*
.then(detail=>{
    detail.point=config.point


})*/
var title_hant=""
var title_en=""
var content_en=""
var content_hant=""
var point_wieghted={
    '50':0.08,
    '60':0.08,
    '70':0.09,
    '80':0.09,
    '90':0.1,
    '100':0.1,
    '110':0.1,
    '120':0.1,
    '130':0.1,
    '140':0.08,
    '150':0.08
}

function randomePoint(){
    return weighted.select(point_wieghted)
}
var lang_array=['en','es','fr','ja','ko','zh_hans','zh_hant']

function test(times){
    var set={}
    for(var i=0;i<times;i++){
        var point=randomePoint()
        if(set[point]!=undefined){
           set[point]+=1
        }else{
            set[point]=1
        }
    }
    console.log(set)

}
