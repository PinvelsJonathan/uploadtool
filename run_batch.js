const csvtojsonV2=require("csvtojson/v2");
const config=require('./batch_config.js')
var weighted = require('weighted')
const readline = require('readline');
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
  });
const createCsvWriter = require('csv-writer').createObjectCsvWriter;
var api=require('./api.js')
var doneCount=0
var todoCount=0
var successCount=0
var failedCount=0
const DETAIL=false
const csvWriter = createCsvWriter({
    path: 'batch_result.csv',
    header: [
        {id:'user',title:'user'},
        {id: 'post_title_en', title: 'post_title_en'},
        {id: 'post_video', title: 'post_video_name'},
        {id:'status',title:'status'},
        {id:'comment',title:'comment'},
        {id:'mtime',title:'mtime'},
        {id:'point',title:'point'},
        {id:'status',title:'status'},
        {id:'lang',title:'lang'}
    ]
});

console.log('\x1b[33m%s\x1b[0m', 'loading csv..')
csvtojsonV2().fromFile('batch.csv').then(obj=>{
    todoCount=obj.length
    console.log(`done reading.Total Count: `+obj.length)
    rl.question( `confirmed? (y,n)`, (answer) => {
        rl.close()
        if(answer=='y')
            upload_many(obj)
        else{
            console.log('not y... aborted')
        }
    })
})

async function upload_many(post_ob_array){
    console.log('\x1b[33m%s\x1b[0m', 'starting upload..')

    post_ob_array.reduce((promise,item)=>{
            return promise.then(a=>{
                return upload_one(item)
                console.log('done 1')
            })
    },Promise.resolve('')).then(a=>{
        console.log('\x1b[33m%s\x1b[0m', `\ntask done  success:${successCount}, failed:${failedCount}, total:${todoCount}\nplease check batch_result.csv for more detail`)
    })
}

async function upload_one(post_obj){
    process.stdout.write(`\rongoing.. ${doneCount}/${todoCount}`);
    if(DETAIL)
    console.log(post_obj)
    var obj={}
    await api.login_user(post_obj.userEmail,config.pw).then(profile=>{
        if(DETAIL)
        console.log('Info- login success')
        obj.profile=profile
        return api.upload_video(config.videoFolderPath+'/'+post_obj.videoFileName+'.mp4')
    }).then(videoRes=>{
        obj.videoRes=videoRes
        var body_obj={
            content:post_obj.desZhHans,
            locale:post_obj.lang,
            location_id:post_obj.locationID,
            title:post_obj.titleZhHans,
            suggested_location:"",
            video:videoRes
        }
        return api.createPost(obj.profile.access_token,body_obj).then(postData=>{
            if(DETAIL){
                  console.log('Info--create Post Done')
                console.log('Info--waiting 5s...')
            }
            return new Promise(done=>setTimeout(done,5000)).then(a=> {return api.getCmsPostDetail(postData.id)})
        })

    }).then(postd=>{
        if(config.published){
            postd.state='published'
        }
        postd.point=randomePoint()
        postd.title.en=post_obj.titleEn
        postd.title.zh_hant=post_obj.titleZhHant
        postd.title.es=post_obj.titleEn
        postd.title.fr=post_obj.titleEn
        postd.title.ja=post_obj.titleEn
        postd.title.ko=post_obj.titleEn
        postd.content.en=post_obj.desEn
        postd.content.zh_hant=post_obj.desZhHant
        postd.content.es=post_obj.desEn
        postd.content.fr=post_obj.desEn
        postd.content.ja=post_obj.desEn
    var string=""
    lang_array.forEach((e)=>{
        string+=postd.location.name[e]        
        string+='\n'
        string+=postd.location.city.name[e]
        string+='\n'
        string+=postd.location.country.name[e]
        string+='\n'
    })
    postd.content.ko=string
    var date =new Date()
    date.setHours(getRandomInt(0,23),getRandomInt(0,59),getRandomInt(0,59))
    //set month random (min,max)
    date.setMonth(getRandomInt(8,9))
    //set date   random (min,max) carefult for 30,31?
    date.setDate(getRandomInt(10,11))
    postd.mtime=date.getTime()
    return api.updateCmsPost(postd)
    }).then(result=>{
        var record={
            user:post_obj.userEmail,
            post_title_en:post_obj.titleEn,
            post_video:post_obj.videoFileName,
            status:'success',
            comment:'',
            mtime:new Date(result.mtime).toLocaleString(),
            point:result.point,
            status:result.state,
            lang:result.locale
        }
        doneCount+=1
        successCount+=1
        return csvWriter.writeRecords([record])

    }).catch(e=>{
        var record={
            user:post_obj.userEmail,
            post_title_en:post_obj.titleEn,
            post_video:post_obj.videoFileName,
            status:'failed',
            comment:e,
        }

        doneCount+=1
        failedCount+=1
        return csvWriter.writeRecords([record])
    })


}

var lang_array=['en','es','fr','ja','ko','zh_hans','zh_hant']

function getRandomInt(min,max){
return Math.floor(Math.random()*(max-min+1))+min
}

var point_wieghted={
    '50':0.08,
    '60':0.08,
    '70':0.09,
    '80':0.09,
    '90':0.1,
    '100':0.1,
    '110':0.1,
    '120':0.1,
    '130':0.1,
    '140':0.08,
    '150':0.08
}

function randomePoint(){
    return weighted.select(point_wieghted)
}